#!/usr/bin/env python

import os
import zipfile

for item in os.listdir('.'):

    filename, fileext = os.path.splitext(item)

    if fileext != '.log':
        continue

    if filename in ['client', 'interface', 'Encounter', 'Encounter.split']:
        continue

    zipfile.ZipFile(filename + '.zip', mode='w', compression=zipfile.ZIP_DEFLATED).write(item)
    os.replace(item, "zipped." + item)
